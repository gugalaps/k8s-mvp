# k8s-mvp

## Описание:
Набор sh-скриптов, генерирующих готовые terraform-теймплейты для создания gke-кластера
Поддерживается возможность создавать/удалять кластер через gitlab ci/cd, а также через ручной запуск скриптов.


## Подготовка
 * Создать Google Cloud Project
 * Завести Service Account (IAM) с ролью Project Owner
 * Скачать Service Account Key (json)
 * Включить Google Cloud Resource Manager API (нужно для terraform import)
    https://console.developers.google.com/apis/library/compute.googleapis.com


## Задачи:
 * Продакшн-кластер может развертываться только из master-ветки / Dev-кластер - из dev-ветки
 * импорт tfstate ресурсов из кластера: https://www.terraform.io/docs/import/index.html
 * в ci/cd игнорировать если tfstate-import выдает ошибку: Cannot import non-existent remote object
 

## Ручное создание кластера:


### 1. Создаем terraform-теймплеты:
* ad-hoc генерация terraform-темплейтов:
```
  cd ./base-layer
  ./gen-adhoc.sh
```
* демо генерация terraform-темплейтов:
```
  cd ./base-layer
  ./gen-demo.sh dev/prod
```

В результате будут созданы директории с файлами:
```
./env
  ./dev
  ./prod
./storage
  ./dev
  ./prod
```

### 2. Настраиваем Env:
 export GOOGLE_APPLICATION_CREDENTIALS="[PATH to Key]"

### 3. Создаем gke-кластер:

  ./apply.sh dev
or
  ./apply.sh prod


### 4. Удаляем gke-кластер:
  ./destroy.sh dev
or
  ./destroy.sh prod


### 5. Удаляем terraform-теймплеты:
  ./gen-cleanup.sh




## Создание кластера через GitLab CI/CD:


### 1. Создаем terraform-теймплеты:
* ad-hoc генерация terraform-темплейтов:
```
  cd ./base-layer
  ./gen-adhoc.sh
```
* демо генерация terraform-темплейтов:
```
  cd ./base-layer
  ./gen-demo.sh dev/prod
```

### 2. Настраиваем GitLab Env с типом File:
 GOOGLE_APPLICATION_CREDENTIALS = <содержимое google service account key>


### 3. git push 

### 4. вручную запускаем pipeline stage: apply