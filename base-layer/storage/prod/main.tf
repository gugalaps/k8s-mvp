
provider google {
  version = "3.5.0"
  project = "feisty-oxide-274811"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_storage_bucket" "my-prod-storage-001" {
  name     = "my-prod-storage-001"
  location = "us-central1"
}

