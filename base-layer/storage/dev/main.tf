
provider google {
  version = "3.5.0"
  project = "feisty-oxide-274811"
  region  = "us-east1"
  zone    = "us-east1-c"
}

resource "google_storage_bucket" "my-dev-storage-001" {
  name     = "my-dev-storage-001"
  location = "us-east1"
  #storage_class = "regional"
  force_destroy = true
}

