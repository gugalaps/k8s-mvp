#!/bin/sh

google_provider_version="3.5.0"
project_id="feisty-oxide-274811"

terraform_version="0.12"
node_pool_autoscaling_min_node_count=4
node_pool_autoscaling_max_node_count=4


kubernetes_version="1.15."

./scripts/create-dirs.sh

#dev
region_id="us-east1"
zone_id="us-east1-c" 
worker_machine_type="n1-standard-2"
cur_environment="dev"
bucket_name="my-dev-storage-001"
cluster_name="development-001"
./scripts/create-storage.sh $cur_environment $google_provider_version $project_id $region_id $zone_id $bucket_name
./scripts/create-dev-cluster.sh $google_provider_version $project_id $region_id $zone_id $terraform_version $cluster_name $node_pool_autoscaling_min_node_count $node_pool_autoscaling_max_node_count $worker_machine_type $bucket_name

#prod
region_id="us-central1"
zone_id="us-central1-c" 
worker_machine_type="n1-standard-2"
cur_environment="prod"
bucket_name="my-prod-storage-001"
cluster_name="production-001"
./scripts/create-storage.sh $cur_environment $google_provider_version $project_id $region_id $zone_id $bucket_name
./scripts/create-prod-cluster.sh $google_provider_version $project_id $region_id $zone_id $terraform_version $cluster_name $node_pool_autoscaling_min_node_count $node_pool_autoscaling_max_node_count $worker_machine_type $bucket_name


./scripts/create-module.sh $project_id $region_id $cluster_name $node_pool_autoscaling_min_node_count $node_pool_autoscaling_max_node_count $worker_machine_type $kubernetes_version
