#!/bin/sh


google_provider_version=$1
project_id=$2
region_id=$3
zone_id=$4
terraform_version=$5
cluster_name=$6
node_pool_autoscaling_min_node_count=$7
node_pool_autoscaling_max_node_count=$8
worker_machine_type=$9
bucket_name=${10}


echo "
terraform {
  required_version = \"~> $terraform_version\"
}

provider google {
  version = \"$google_provider_version\"
  project = \"$project_id\"
  region  = \"$region_id\"
  zone    = \"$zone_id\"
}

provider google-beta {
  version = \"$google_provider_version\"
  project = \"$project_id\"
  region  = \"$region_id\"
  zone    = \"$zone_id\"
}

# Reserving a new static external IP address
# https://cloud.google.com/compute/docs/ip-addresses/reserve-static-external-ip-address#reserve_new_static
resource google_compute_address \"staticIP\" {
  name         = \"$cluster_name\"
  project      = \"$project_id\"
  region       = \"$region_id\"
  address_type = \"EXTERNAL\"
}

module prod_environment {
  source                               = \"../../module\"
  project                              = \"$project_id\"
  region                               = \"$region_id\"
  name                                 = \"$cluster_name\"
  node_pool_autoscaling_min_node_count = $node_pool_autoscaling_min_node_count
  node_pool_autoscaling_max_node_count = $node_pool_autoscaling_max_node_count
  worker_machine_type                  = \"$worker_machine_type\"
}" >./env/prod/main.tf


echo "
terraform {
    backend \"gcs\" {
      bucket = \"$bucket_name\"   
      prefix = \"terraform/state\"
    }
}" >./env/prod/backend.tf


echo "
output prod_output {
  value = module.prod_environment
}
" >./env/prod/outputs.tf


echo "
#https://www.terraform.io/docs/providers/google/r/compute_address.html
terraform import google_compute_address.staticIP \"$project_id\"/\"$region_id\"/\"$cluster_name\"
" >./env/prod/import.sh

