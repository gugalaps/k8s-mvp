#!/bin/sh

cur_environment=$1
google_provider_version=$2
project_id=$3
region_id=$4
zone_id=$5
bucket_name=$6


if [[ "$cur_environment" == "dev" ]]; then
echo "
provider google {
  version = \"$google_provider_version\"
  project = \"$project_id\"
  region  = \"$region_id\"
  zone    = \"$zone_id\"
}

resource \"google_storage_bucket\" \"$bucket_name\" {
  name     = \"$bucket_name\"
  location = \"$region_id\"
  #storage_class = \"regional\"
  force_destroy = true
}
" >./storage/dev/main.tf

echo "
terraform import google_storage_bucket.$bucket_name \"$project_id\"/\"$bucket_name\"
">./storage/dev/import.sh
fi


if [[ "$cur_environment" == "prod" ]]; then
  echo "
provider google {
  version = \"$google_provider_version\"
  project = \"$project_id\"
  region  = \"$region_id\"
  zone    = \"$zone_id\"
}

resource \"google_storage_bucket\" \"$bucket_name\" {
  name     = \"$bucket_name\"
  location = \"$region_id\"
}
" >./storage/prod/main.tf

echo "
terraform import google_storage_bucket.$bucket_name \"$project_id\"/\"$bucket_name\"
">./storage/prod/import.sh
fi
