
terraform {
    backend "gcs" {
      bucket = "my-prod-storage-001"   
      prefix = "terraform/state"
    }
}
