
terraform {
  required_version = "~> 0.12"
}

provider google {
  version = "3.5.0"
  project = "feisty-oxide-274811"
  region  = "us-east1"
  zone    = "us-east1-c"
}

provider google-beta {
  version = "3.5.0"
  project = "feisty-oxide-274811"
  region  = "us-east1"
  zone    = "us-east1-c"
}


# Reserving a new static external IP address
# https://cloud.google.com/compute/docs/ip-addresses/reserve-static-external-ip-address#reserve_new_static
resource google_compute_address "staticIP" {
  name         = "development-001"
  project      = "feisty-oxide-274811"
  region       = "us-east1"
  address_type = "EXTERNAL"
}

module develop_environment {
  source                               = "../../module"
  project                              = "feisty-oxide-274811"
  region                               = "us-east1"
  name                                 = "development-001"
  node_pool_autoscaling_min_node_count = 4
  node_pool_autoscaling_max_node_count = 4
  worker_machine_type                  = "n1-standard-2"
}
