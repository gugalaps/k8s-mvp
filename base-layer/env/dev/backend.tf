
terraform {
    backend "gcs" {
      bucket = "my-dev-storage-001"   
      prefix = "terraform/state"
    }
}
