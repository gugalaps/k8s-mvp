
variable project {
    description = "GCP project ID"
    type        = string
    default     = "feisty-oxide-274811"
}

variable region {
    description = "GCP region"
    type        = string
    default     = "us-central1"
}

variable name {
    description = "Environment name"
    type        = string
    default     = "production-001"
}

variable node_pool_autoscaling_min_node_count {
    description = "Minimum nodes in cluster = value * availability zones count"
    type        = number
    default     = 4
}

variable node_pool_autoscaling_max_node_count {
    description = "Maximum nodes in cluster = value * availability zones count"
    type        = number
    default     = 4
}

variable worker_machine_type {
    description = "Worker node's machine type"
    type        = string
    default     = "n1-standard-2"
}

variable worker_disk_size {
  description = "Boot disk size (GB) for worker nodes"
  type        = number
  default     = 20
}

