17.04.20 (Apr 17, 2020)

BUG FIXES:
* err: Terraform Resource Already Exists: google_compute_address при добавлении/удалении кластера: import.sh
* err: GitLab CI Running shell script file not found exit code 127 - добавление #!/bin/sh вместо #!/bin/bash
